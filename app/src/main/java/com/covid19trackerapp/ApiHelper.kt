package com.covid19trackerapp

object ApiHelper {
//    var ipaddress:String = "167.172.152.53"
    var ipaddress:String = ""
    var baseUrl = ""
//    var baseUrl = "http://".plus(ipaddress).plus(":8002")
//    val baseUrl = "http://172.20.10.10:8002"
    val REGISTER = Utils.myserver.plus("/patients/register")
    var LOGIN = "/authentications/patients/login"
    var PATIENT_INFO = Utils.myserver.plus("/patients/")
    var CHANGE_PASSWORD = Utils.myserver.plus("/patients/changePassword/")
    var UPDATE_INFO = Utils.myserver.plus("/patients/update/")
    var PATIENT_QUARANTINE = Utils.myserver.plus("/patientQuarantineSchedules")
    var QUARANTINE_CREATE = Utils.myserver.plus("/patientQuarantinedLocations/create")
}
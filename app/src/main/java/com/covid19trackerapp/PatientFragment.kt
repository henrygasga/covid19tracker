package com.covid19trackerapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import androidx.fragment.app.Fragment
import com.covid19trackerapp.ObjectSingleton.loginData
import com.covid19trackerapp.databinding.FragmentMapBinding
import com.covid19trackerapp.databinding.FragmentPatientBinding
import com.covid19trackerapp.interfaces.PatientInterface
import com.covid19trackerapp.models.PatientProfileData
import com.covid19trackerapp.presenters.PatientPresenter

class PatientFragment : Fragment(), PatientInterface.PatientInfoView {

    private lateinit var binding: FragmentPatientBinding
    lateinit var loadingDialog: AppCompatDialog
    lateinit var presenter: PatientPresenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPatientBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingDialog = DialogFactory.showLoadingDialog(requireActivity())
        presenter = PatientPresenter()
        presenter.patientView = this
        binding.run {
            logoutBtn.setOnClickListener {
                requireActivity().launchActivity<LoginActivity> {  }
                requireActivity().finish()
            }
            editPatient.setOnClickListener {
                requireActivity().launchActivity<EditProfileActivity> {  }
            }
            changePass.setOnClickListener {
                requireActivity().launchActivity<ChangePassActivity> {  }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getPatientInfo(loginData!!.data!!.id.toString())
    }

    override fun onFailed(msg: String) {
        loadingDialog.dismiss()
        requireActivity().toastValidation(msg)
    }

    override fun onGetPatient(res: PatientProfileData) {
        loadingDialog.dismiss()
        binding.run {
            model = res
            executePendingBindings()
        }
    }
}
package com.covid19trackerapp.models

data class RegisterResData(
    val errors: List<String>?,
    val message: String?,
    val status: String?,
    val statusCode: Int?,
    val result: ResultRegister?
)

class ChangePassDatas : ArrayList<Any>()

data class ResultRegister(
    val contact_no: String?,
    val created_at: String?,
    val created_user_id: Int?,
    val date_approved: Any?,
    val email: String?,
    val firstname: String?,
    val gender_type: Int?,
    val id: Int?,
    val is_active: String?,
    val is_logged: String?,
    val lastname: String?,
    val middlename: String?,
    val primary_address: String?,
    val secondary_address: Any?,
    val status: Int?
)

data class LoginResult(
    val `data`: DataLogin?,
    val expiresIn: String?,
    val token: String?
)

data class PatientProfileData(
    val contact_no: String?,
    val created_at: String?,
    val created_user_id: Int?,
    val date_approved: Any?,
    val email: String?,
    val file_path: Any?,
    val firstname: String?,
    val gender_type: Int?,
    val id: Int?,
    val is_active: Int?,
    val lastname: String?,
    val middlename: String?,
    val patient_no: String?,
    val primary_address: String?,
    val secondary_address: Any?,
    val status: Int?,
    val updated_at: String?,
    val updated_user_id: Int?,
    var mygender:String? = "male"
)

data class DataLogin(
    val contact_no: String?,
    val date_approved: Any?,
    val email: String?,
    val file_path: Any?,
    val firstname: String?,
    val gender_type: Int?,
    val id: Int?,
    val lastname: String?,
    val middlename: String?,
    val patient_no: String?,
    val primary_address: String?,
    val secondary_address: Any?
)

class QuarantineSchedData :  ArrayList<ResultQuarantineSched>()

data class SendLocationData(
    val address: String?,
    val created_at: String?,
    val id: Int?,
    val latitude: String?,
    val longitude: String?,
    val patient_id: Int?,
    val patient_quarantine_schedule_id: Int?
)

data class ResultQuarantineSched(
    val created_at: String?,
    val created_user_id: Int?,
    val date_ended: String?,
    val date_started: String?,
    val id: Int?,
    val patient_id: Int?,
    val patients: Patients?,
    val remarks: String?,
    val status: Int?,
    val updated_at: Any?,
    val updated_user_id: Any?
)

data class Patients(
    val contact_no: String?,
    val date_approved: String?,
    val email: String?,
    val file_path: Any?,
    val firstname: String?,
    val gender_type: Int?,
    val is_active: Int?,
    val lastname: String?,
    val middlename: String?,
    val patient_no: String?,
    val primary_address: String?,
    val secondary_address: Any?,
    val status: Int?
)

data class PatientUpdateData(
    val contact_no: String?,
    val created_at: String?,
    val created_user_id: Int?,
    val date_approved: Any?,
    val email: String?,
    val file_path: Any?,
    val firstname: String?,
    val gender_type: Int?,
    val id: Int?,
    val is_active: Int?,
    val lastname: String?,
    val middlename: String?,
    val patient_no: String?,
    val primary_address: String?,
    val secondary_address: Any?,
    val status: Int?,
    val updated_at: String?,
    val updated_user_id: String?
)

class GeoMappingRadiusRes : ArrayList<GeoMappingRadiusResItem>()

data class GeoMappingRadiusResItem(
    val contact_no: String?,
    val created_at: String?,
    val id: Int?,
    val latitude: String?,
    val longitude: String?,
    val name: String?,
    val radius: String?
)

data class NotifCreateData(
    val address: String?,
    val created_at: String?,
    val geomappingRadiusLocations: GeomappingRadiusLocations?,
    val geomapping_radius_location_id: Int?,
    val id: Int?,
    val is_outside: Int?,
    val latitude: String?,
    val longitude: String?,
    val patient_id: Int?,
    val patients: Patients?
)

data class GeomappingRadiusLocations(
    val contact_no: String?
)

//data class Patients(
//    val email: String?,
//    val firstname: String?,
//    val lastname: String?,
//    val middlename: String?,
//    val patient_no: String?
//)

data class BaseApiResponse<T>(
    val result: T,
    val message: String?,
    val status: String?,
    val statusCode: Int?,
    val errors: List<String>?
)
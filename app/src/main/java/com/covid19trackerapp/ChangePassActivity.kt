package com.covid19trackerapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import com.covid19trackerapp.ObjectSingleton.loginData
import com.covid19trackerapp.databinding.ActivityChangePassBinding
import com.covid19trackerapp.interfaces.PatientInterface
import com.covid19trackerapp.presenters.PatientPresenter

class ChangePassActivity : AppCompatActivity(), PatientInterface.ChangePassView {
    private val binding: ActivityChangePassBinding by contentView(R.layout.activity_change_pass)
    lateinit var loadingDialog: AppCompatDialog
    lateinit var presenter: PatientPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = DialogFactory.showLoadingDialog(this@ChangePassActivity)
        presenter = PatientPresenter()
        presenter.changePassView = this
        binding.run {
            backImage.setOnClickListener {
                finish()
            }

            submitBtn.setOnClickListener {
                validate()
            }
        }
    }

    fun validate() {
        binding.run {
            if(oldPass.text.isNullOrEmpty() ||
                confirmPass.text.isNullOrEmpty() ||
                newPass.text.isNullOrEmpty() ) {
                toastValidation("All fields required")
            } else if(newPass.text.toString() != confirmPass.text.toString()) {
                toastValidation("Password did not match.")
            } else {
                loadingDialog.show()
                presenter.changePassword(
                    loginData!!.data!!.id.toString(),
                    oldPass.text.toString(),
                    newPass.text.toString()
                )
            }
        }
    }

    override fun onFailed(msg: String) {
        loadingDialog.dismiss()
        toastValidation(msg)
    }

    override fun onChangePass() {
        loadingDialog.dismiss()
        ObjectSingleton.mypass = binding.newPass.text.toString()
        toastValidation("Change password success.")
        finish()
    }
}
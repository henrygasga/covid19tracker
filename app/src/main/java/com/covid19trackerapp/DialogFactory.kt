package com.covid19trackerapp

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatDialog

class DialogFactory {
    companion object {
        @JvmStatic
        fun showLoadingDialog(context: Context): AppCompatDialog {
            val v: View = LayoutInflater.from(context).inflate(R.layout.loading, null)
            val dialog = AppCompatDialog(context)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setContentView(v)
            return dialog
        }
    }
}
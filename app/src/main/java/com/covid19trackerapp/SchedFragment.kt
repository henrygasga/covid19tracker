package com.covid19trackerapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import androidx.fragment.app.Fragment
import com.covid19trackerapp.databinding.FragmentPatientBinding
import com.covid19trackerapp.databinding.FragmentSchedBinding
import com.covid19trackerapp.interfaces.PatientInterface
import com.covid19trackerapp.models.QuarantineSchedData
import com.covid19trackerapp.models.ResultQuarantineSched
import com.covid19trackerapp.presenters.PatientPresenter

class SchedFragment : Fragment(), PatientInterface.QuarantineSchedView {
    private lateinit var binding: FragmentSchedBinding
    lateinit var loadingDialog: AppCompatDialog
    lateinit var presenter: PatientPresenter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSchedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingDialog = DialogFactory.showLoadingDialog(requireActivity())
        presenter = PatientPresenter()
        presenter.quarantineSchedView = this
        loadingDialog.show()
        presenter.getQuarantineSched()
        binding.run {

        }
    }

    override fun onFailed(msg: String) {
        loadingDialog.dismiss()
        requireActivity().toastValidation(msg)
    }

    override fun onGetQuarantineSched(res: QuarantineSchedData) {
        loadingDialog.dismiss()
        if(res.size > 0) {
            for(i: ResultQuarantineSched in res) {
                if(ObjectSingleton.loginData!!.data!!.id == i.patient_id) {
                    binding.run {
                        startDate.text = Utils.convertDateWithTimeZone(i.date_started!!)
                        endDate.text = Utils.convertDateWithTimeZone(i.date_ended!!)
                    }
                    break
                }
            }
        } else {
            requireActivity().toastValidation("No Records")
        }
    }
}
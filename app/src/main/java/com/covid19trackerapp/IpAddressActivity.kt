package com.covid19trackerapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.covid19trackerapp.ApiHelper.baseUrl
import com.covid19trackerapp.ApiHelper.ipaddress
import com.covid19trackerapp.Utils.Companion.myserver
import com.covid19trackerapp.databinding.ActivityIpaddressBinding

class IpAddressActivity : AppCompatActivity() {
    private val binding: ActivityIpaddressBinding by contentView(R.layout.activity_ipaddress)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.run {
            setBtn.setOnClickListener {
                if(!email.text.isNullOrEmpty()) {
                    ipaddress = email.text.toString()
                    myserver = "http://${email.text.toString()}:8002"
                    launchActivity<LoginActivity> {  }
                    finish()
                }
            }
        }
    }
}
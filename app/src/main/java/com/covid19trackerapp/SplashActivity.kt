package com.covid19trackerapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.covid19trackerapp.databinding.ActivitySplashBinding
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SplashActivity : AppCompatActivity(), CoroutineScope {

    private val binding: ActivitySplashBinding by contentView(R.layout.activity_splash)
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.run {
            launch {
                delay(1000)
                withContext(Dispatchers.Main) {
                    launchActivity<IpAddressActivity> {  }
                    finish()
                }
            }
        }
    }
}
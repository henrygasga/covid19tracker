package com.covid19trackerapp

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.covid19trackerapp.databinding.FragmentMapBinding
import com.covid19trackerapp.interfaces.PatientInterface
import com.covid19trackerapp.models.*
import com.covid19trackerapp.presenters.PatientPresenter
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.util.*
import kotlin.random.Random

class MapFragment : Fragment(), OnMapReadyCallback, PatientInterface.LocationCreateView, PatientInterface.QuarantineSchedView,
    PatientInterface.GeomappingRadiusLocationsView, PatientInterface.CreateNotificationView {
    private lateinit var binding: FragmentMapBinding
    private lateinit var mMap: GoogleMap
    private lateinit var mapFragment: SupportMapFragment
    private var mLocationRequest: LocationRequest? = null
    private var mLocationCalback: LocationCallback? = null
    lateinit var fusedClient: FusedLocationProviderClient
    var myLoc: Location? = null
    lateinit var presenter: PatientPresenter
    lateinit var mainHandler: Handler
    var quaSched:ResultQuarantineSched? = null
    var loadedMap = false
    var currentRadius:GeoMappingRadiusResItem? = null
    var myCircle:Circle? = null
    var notifSend = false
    var notifApiSend = false

    private val updateTextTask = object : Runnable {
        override fun run() {
            presenter.getQuarantineSched()
            presenter.getGeomappingRadiusLocations()
            fetchLastLocation()
            mainHandler.postDelayed(this, 5000)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainHandler = Handler(Looper.getMainLooper())
        presenter = PatientPresenter()
        presenter.locationView = this
        presenter.quarantineSchedView = this
        presenter.geomappingRadiusLocationsView = this
        presenter.createNotificationView = this
        fusedClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        mLocationRequest = LocationRequest.create()
        mLocationRequest!!.interval = 60000
        mLocationRequest!!.fastestInterval = 5000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        mLocationCalback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                if (p0 == null) {
                    return
                }
                for(loc:Location in p0.locations) {
                    if (loc != null) {
//                        toastValidation("lat: ${loc.latitude}, lng: ${loc.longitude}")
                        myLoc = loc
                        setLocation()
                        break
                    }
                }
            }
        }
        binding.run {
            mapFragment = childFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
            mapFragment.getMapAsync(this@MapFragment)
        }

//        presenter.getGeomappingRadiusLocations()
    }

    override fun onResume() {
        super.onResume()
        mainHandler.post(updateTextTask)
        fetchLastLocation()
    }

    override fun onPause() {
        super.onPause()
        mainHandler.removeCallbacks(updateTextTask)
    }

    override fun onGeomappingRadiusLocations(res: GeoMappingRadiusRes) {
        if(res.isNotEmpty()) {
            for(i: GeoMappingRadiusResItem in res) {
                val myDistance = FloatArray(2)
                var curCircle:Circle? = null
                if(mMap != null) {
                    var clatlng = ""
                    if(myCircle != null) {
                        clatlng = "${myCircle!!.center.latitude}"
                    }
                    if(clatlng == "") {

                        curCircle = mMap.addCircle(CircleOptions()
                            .center(LatLng(i.latitude!!.toDouble(), i.longitude!!.toDouble()))
                            .radius(i.radius!!.toDouble())
                            .strokeColor(Color.BLACK)
                            .fillColor(0x30ff0000)
                        )
                    } else {
                        if(currentRadius == null) {
                            if(clatlng != "${i.latitude}") {
                                mMap.clear()
                                if(loadedMap) {
                                    mMap.addMarker(
                                        MarkerOptions()
                                            .position(LatLng(myLoc!!.latitude, myLoc!!.longitude))
                                            .title("You are here!"))
                                }
                                curCircle = mMap.addCircle(CircleOptions()
                                    .center(LatLng(i.latitude!!.toDouble(), i.longitude!!.toDouble()))
                                    .radius(i.radius!!.toDouble())
                                    .strokeColor(Color.BLACK)
                                    .fillColor(0x30ff0000)
                                )
                            }
                        } else {
                            if(currentRadius!!.id == i.id) {
                                if(clatlng != "${i.latitude}") {
                                    mMap.clear()
                                    if(loadedMap) {
                                        mMap.addMarker(
                                            MarkerOptions()
                                                .position(LatLng(myLoc!!.latitude, myLoc!!.longitude))
                                                .title("You are here!"))
                                    }
                                    curCircle = mMap.addCircle(CircleOptions()
                                        .center(LatLng(i.latitude!!.toDouble(), i.longitude!!.toDouble()))
                                        .radius(i.radius!!.toDouble())
                                        .strokeColor(Color.BLACK)
                                        .fillColor(0x30ff0000)
                                    )
                                }
                            }
                        }

                    }



                    if(myLoc != null) {
                        if(curCircle != null) {


                            if(clatlng != "${curCircle.center.latitude}") {
                                Location.distanceBetween(myLoc!!.latitude, myLoc!!.longitude, curCircle.center.latitude, curCircle.center.longitude, myDistance)
//                            if(myDistance[0] > curCircle.radius) {
//
//                            } else {
                                if(currentRadius == null) {
                                    if(notifSend) {
                                        notifSend = false
                                    }
                                    if(notifApiSend) {
                                        notifApiSend = false
                                    }
                                    currentRadius = i
                                    myCircle = curCircle
                                } else {
                                    if(currentRadius!!.id == i.id) {
                                        if(notifSend) {
                                            notifSend = false
                                        }
                                        if(notifApiSend) {
                                            notifApiSend = false
                                        }
                                        currentRadius = i
                                        myCircle = curCircle
                                    }
                                }

//                            }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onCreateNotification(res: NotifCreateData) {

    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isZoomGesturesEnabled = true
        mMap.uiSettings.isScrollGesturesEnabled = true
        mMap.uiSettings.isRotateGesturesEnabled = false
        mMap.uiSettings.isZoomControlsEnabled = false
    }

    fun fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            initLocationPermission()
            return
        }
        fusedClient.lastLocation.addOnSuccessListener {
            if (it != null) {
                myLoc = it
                setLocation()
            } else {
                fusedClient.requestLocationUpdates(mLocationRequest, mLocationCalback!!, null)
            }
        }
    }

    override fun onFailed(msg: String) {

    }

    override fun onSendLocation(res: SendLocationData) {

    }

    override fun onGetQuarantineSched(res: QuarantineSchedData) {
        if(res.size > 0) {
            for(i: ResultQuarantineSched in res) {
                if(ObjectSingleton.loginData!!.data!!.id == i.patient_id) {
                    quaSched = i
                    break
                }
            }
        } else {
            requireActivity().toastValidation("No Records")
        }
    }

    fun setLocation() {
        mMap.setOnMapLoadedCallback {
            if(myLoc != null) {

                try {
                    val gcd: Geocoder = Geocoder(
                        requireActivity(),
                        Locale.getDefault())
                    val addreses: List<Address>
                    var currentAddress: String = ""
                    addreses = gcd.getFromLocation(myLoc!!.latitude,
                        myLoc!!.longitude, 1)
                    if(quaSched != null) {
                        if (addreses.size > 0) {
                            val address = addreses[0].getAddressLine(0)
                            val locallity = addreses[0].locality
                            val sublocality = addreses[0].subLocality
                            if (sublocality != null) {
                                currentAddress = StringBuilder(locallity).append(",").append(sublocality).toString()
                            } else {
                                currentAddress = locallity
                            }
                            val checkDistance = FloatArray(2)
                            if(myCircle != null) {
                                Location.distanceBetween(myLoc!!.latitude, myLoc!!.longitude, myCircle!!.center.latitude, myCircle!!.center.longitude, checkDistance)
                                if(checkDistance[0] > myCircle!!.radius) {
                                    //Outside radius
                                    if(!notifSend) {
                                        notifSend = true
                                        Common.showNotification(requireActivity(), Random.nextInt(),
                                            "Covid19 Tracker",
                                            "You are outside the radius!",
                                            Intent(requireActivity(),
                                                SplashActivity::class.java)
                                        )

                                        if(currentRadius != null) {
                                            presenter.createNotification(
                                                currentAddress, LatLng(myLoc!!.latitude, myLoc!!.longitude),
                                                1, ObjectSingleton.loginData!!.data!!.id!!,
                                                currentRadius!!.id!!
                                            )
                                        }
                                    }
                                } else {
                                    //Inside Radius
                                    if(!notifApiSend) {
                                        notifApiSend = true
                                        Common.showNotification(requireActivity(), Random.nextInt(),
                                            "Covid19 Tracker",
                                            "You are inside the radius!",
                                            Intent(requireActivity(),
                                                SplashActivity::class.java)
                                        )
                                        if(currentRadius != null) {
                                            presenter.createNotification(
                                                currentAddress, LatLng(myLoc!!.latitude, myLoc!!.longitude),
                                                0, ObjectSingleton.loginData!!.data!!.id!!,
                                                currentRadius!!.id!!
                                            )
                                        }
                                    }
                                }
                            }

                            presenter.sendLocation(
                                LatLng(myLoc!!.latitude, myLoc!!.longitude),
                                currentAddress,
                                ObjectSingleton.loginData!!.data!!.id!!,
                                quaSched!!.id!!
                            )
                        }
                    }
                } catch (e: Exception) {

                }

                if(!loadedMap) {
                    loadedMap = true
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(myLoc!!.latitude, myLoc!!.longitude), 18f))
                    mMap.addMarker(
                        MarkerOptions()
                            .position(LatLng(myLoc!!.latitude, myLoc!!.longitude))
                            .title("You are here!"))
                }
            }
        }
    }

    private fun initLocationPermission() {
        Dexter.withActivity(requireActivity())
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        fetchLastLocation()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    showRationalDialogForPermissions()
                }
            }).check()
    }

    private fun showRationalDialogForPermissions() {
//        if(!(applicationContext as Activity).isFinishing) { // here fix
        AlertDialog.Builder(requireActivity())
            .setMessage(R.string.go_permission_settings)
            .setPositiveButton(R.string.str_go_settings) { dialog, which -> openAppPermissionSettings() }
            .setNegativeButton(R.string.cancel) { dialog, which -> dialog.dismiss() }.show()
//        }

    }

    protected fun openAppPermissionSettings() {
        try {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", requireActivity().packageName, null)
            intent.data = uri
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        when (requestCode) {
            123 -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] === PackageManager.PERMISSION_DENIED) {
                    // permission was denied, show alert to explain permission
                    showRationalDialogForPermissions()
                } else {
                    //permission is granted now start a background service
                    if (ActivityCompat.checkSelfPermission(
                            requireActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(
                            requireActivity(),
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        // permison accept
                        fetchLastLocation()
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
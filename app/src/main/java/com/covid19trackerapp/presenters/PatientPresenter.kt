package com.covid19trackerapp.presenters

import android.util.Log
import com.covid19trackerapp.ApiHelper
import com.covid19trackerapp.ObjectSingleton
import com.covid19trackerapp.ObjectSingleton.mypass
import com.covid19trackerapp.Utils
import com.covid19trackerapp.interfaces.PatientInterface
import com.covid19trackerapp.models.*
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.gson.jsonBody
import com.github.kittinunf.fuel.gson.responseObject
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import okhttp3.internal.http2.Header
import org.json.JSONObject

class PatientPresenter : PatientInterface.Presenter {
    var server:String? = null
    var registerView: PatientInterface.RegisterView? = null
    var loginView: PatientInterface.LoginView? = null
    var patientView: PatientInterface.PatientInfoView? = null
    var changePassView: PatientInterface.ChangePassView? = null
    var updateInfoView: PatientInterface.UpdatePatientView? = null
    var quarantineSchedView: PatientInterface.QuarantineSchedView? = null
    var locationView: PatientInterface.LocationCreateView? = null
    var notificationView: PatientInterface.NotificationView? = null
    var geomappingRadiusLocationsView: PatientInterface.GeomappingRadiusLocationsView? = null
    var createNotificationView: PatientInterface.CreateNotificationView? = null

    override fun createNotification(
        address: String,
        latLng: LatLng,
        is_outside: Int,
        patient_id: Int,
        geomapping_radius_location_id: Int
    ) {
        val json = JSONObject()
        json.put("address", address)
        json.put("latitude", latLng.latitude.toString())
        json.put("longitude", latLng.longitude.toString())
        json.put("is_outside", is_outside)
        json.put("patient_id", patient_id)
        json.put("geomapping_radius_location_id", geomapping_radius_location_id)
        Fuel.post(Utils.myserver.plus("/patientQuarantinedNotifications/create"))
            .body(json.toString())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer ${ObjectSingleton.loginData!!.token}")
            .responseObject<BaseApiResponse<NotifCreateData>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                createNotificationView?.onCreateNotification(bytes.result)
                            } else {
                                createNotificationView?.onFailed(bytes.message!!)
                            }
                        }
                    },
                    failure = {
                        createNotificationView?.onFailed(it.message!!)
                    }
                )
            }
    }

    override fun getGeomappingRadiusLocations() {
        Fuel.get(Utils.myserver.plus("/geomappingRadiusLocations"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer ${ObjectSingleton.loginData!!.token}")
            .responseObject<BaseApiResponse<GeoMappingRadiusRes>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                geomappingRadiusLocationsView?.onGeomappingRadiusLocations(bytes.result)
                            } else {
                                geomappingRadiusLocationsView?.onFailed(bytes.message!!)
                            }
                        }
                    },
                    failure = {
                        geomappingRadiusLocationsView?.onFailed(it.message!!)
                    }
                )
            }
    }

    override fun getPatientsNotification(id: String) {
        Fuel.get(Utils.myserver.plus("/patientQuarantinedNotifications/$id"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer ${ObjectSingleton.loginData!!.token}")
            .responseObject<BaseApiResponse<QuarantineSchedData>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                notificationView?.onGetNotification()
                            } else {
                                notificationView?.onFailed(bytes.message!!)
                            }
                        }
                    },
                    failure = {
                        notificationView?.onFailed(it.message!!)
                    }
                )
            }
    }

    override fun sendLocation(
        latLng: LatLng,
        address: String,
        patient_id: Int,
        patient_quarantine_schedule_id: Int
    ) {
        val json = JSONObject()
        json.put("address", address)
        json.put("latitude", latLng.latitude.toString())
        json.put("longitude", latLng.longitude.toString())
        json.put("patient_id", patient_id)
        json.put("patient_quarantine_schedule_id", patient_quarantine_schedule_id)
        Fuel.post(Utils.myserver.plus("/patientQuarantinedLocations/create"))
            .body(json.toString())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer ${ObjectSingleton.loginData!!.token}")
            .responseObject<BaseApiResponse<SendLocationData>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                locationView?.onSendLocation(bytes.result)
                            } else {
                                locationView?.onFailed(bytes.message!!)
                            }
                        }
                    },
                    failure = {
                        locationView?.onFailed(it.message!!)
                    }
                )
            }
    }

    override fun getQuarantineSched() {
        Fuel.get(Utils.myserver.plus("/patientQuarantineSchedules"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer ${ObjectSingleton.loginData!!.token}")
            .responseObject<BaseApiResponse<QuarantineSchedData>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                quarantineSchedView?.onGetQuarantineSched(bytes.result)
                            } else {
                                quarantineSchedView?.onFailed(bytes.message!!)
                            }
                        }
                    },
                    failure = {
                        quarantineSchedView?.onFailed(it.message!!)
                    }
                )
            }
    }

    override fun updateProfile(
        id: String,
        firstname: String,
        middlename: String,
        lastname: String,
        email: String,
        password: String,
        primary_address: String,
        secondary_address: String,
        contact_no: String,
        gender_type: Int
    ) {
        val json = JSONObject()
        json.put("firstname", firstname)
        json.put("middlename", middlename)
        json.put("lastname", lastname)
        json.put("email", email)
        json.put("primary_address", primary_address)
        json.put("secondary_address", secondary_address)
        json.put("contact_no", contact_no)
        json.put("gender_type", gender_type)
        Fuel.put(Utils.myserver.plus("/patients/update/$id"))
            .body(json.toString())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer ${ObjectSingleton.loginData!!.token}")
            .responseObject<BaseApiResponse<PatientUpdateData>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                updateInfoView?.onUpdatePatient(bytes.result)
                            } else {
//                                updateInfoView?.onFailed(bytes.errors!![0])
                                updateInfoView?.onFailed(bytes.message!!)
                            }
                        }
                    },
                    failure = {
                        updateInfoView?.onFailed(it.message!!)
                    }
                )
            }
    }

    override fun register(
        firstname: String,
        middlename: String,
        lastname: String,
        email: String,
        password: String,
        primary_address: String,
        secondary_address: String,
        contact_no: String,
        gender_type: Int
    ) {
        val json = JSONObject()
        json.put("firstname", firstname)
        json.put("middlename", middlename)
        json.put("lastname", lastname)
        json.put("email", email)
        json.put("password", password)
        json.put("primary_address", primary_address)
        json.put("secondary_address", secondary_address)
        json.put("contact_no", contact_no)
        json.put("gender_type", gender_type)
        Fuel.post(Utils.myserver.plus("/patients/register"))
            .body(json.toString())
            .header("Content-Type", "application/json")
            .responseObject< BaseApiResponse<ResultRegister>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                registerView?.onRegisterSuccess(bytes.result)
                            } else {
                                registerView?.onFailed(bytes.errors!![0])
                            }
                        }
                    },
                    failure = {
                        registerView?.onFailed(it.message!!)
                    }
                )
            }
    }

    override fun login(email: String, password: String) {
        val json = JSONObject()
        json.put("email", email)
        json.put("password", password)
        Fuel.post(Utils.myserver.plus("/authentications/patients/login"))
            .body(json.toString())
            .header("Content-Type", "application/json")
            .responseObject< BaseApiResponse<LoginResult>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                loginView?.onLoginSuccess(bytes.result)
                            } else {
                                loginView?.onFailed(bytes.errors!![0])
                            }
                        }
                    },
                    failure = {
                        loginView?.onFailed(it.message!!)
                    }
                )
            }
    }

    override fun getPatientInfo(id: String) {
        Fuel.get(Utils.myserver.plus("/patients/$id"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer ${ObjectSingleton.loginData!!.token}")
            .responseObject<BaseApiResponse<PatientProfileData>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                patientView?.onGetPatient(bytes.result)
                            } else {
                                patientView?.onFailed(bytes.message!!)
                            }
                        }
                    },
                    failure = {
                        patientView?.onFailed(it.message!!)
                    }
                )
            }
    }

    override fun changePassword(id: String, old_password: String, new_password: String) {
        val json = JSONObject()
        json.put("old_password", old_password)
        json.put("new_password", new_password)
        Fuel.put(Utils.myserver.plus("/patients/changePassword/$id"))
            .body(json.toString())
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer ${ObjectSingleton.loginData!!.token}")
            .responseObject<BaseApiResponse<ChangePassDatas>> { request, response, result ->
                result.fold(
                    success = {
                        val (bytes, error) = result
                        if(bytes != null) {
                            if(bytes.status == "success") {
                                changePassView?.onChangePass()
                            } else {
                                changePassView?.onFailed(bytes.message!!)
                            }
                        }
                    },
                    failure = {
                        changePassView?.onFailed(it.message!!)
                    }
                )
            }
    }
}
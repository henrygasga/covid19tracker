package com.covid19trackerapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.covid19trackerapp.databinding.ActivityDashboardBinding
import com.covid19trackerapp.interfaces.PatientInterface
import com.covid19trackerapp.presenters.PatientPresenter
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlin.random.Random

class DashboardActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener, PatientInterface.NotificationView {

    private val binding: ActivityDashboardBinding by contentView(R.layout.activity_dashboard)
    lateinit var presenter: PatientPresenter
    lateinit var mainHandler: Handler

    private val updateTextTask = object : Runnable {
        override fun run() {

            mainHandler.postDelayed(this, 2000)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainHandler = Handler(Looper.getMainLooper())
        presenter = PatientPresenter()
        presenter.notificationView = this
        binding.run {
            navView.setOnNavigationItemSelectedListener(this@DashboardActivity)
            navigateTo(ScreenType.MAP)
        }


    }

    fun navigateTo(screen: ScreenType) {
        binding.run {
            var fragment: Fragment = MapFragment()
            when(screen) {
                ScreenType.PATIENT -> {
                    fragment = PatientFragment()
                }
                ScreenType.SCHED -> {
                    fragment = SchedFragment()
                }
                else -> {

                }
            }

            supportFragmentManager.beginTransaction()
                .replace(R.id.frame_layout_content, fragment)
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onFailed(msg: String) {

    }

    override fun onGetNotification() {

    }

    override fun onResume() {
        super.onResume()
        mainHandler.post(updateTextTask)
    }

    override fun onPause() {
        super.onPause()
        mainHandler.removeCallbacks(updateTextTask)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.nav_map -> {
                navigateTo(ScreenType.MAP)
                return true
            }
            R.id.nav_patient -> {
                navigateTo(ScreenType.PATIENT)
                return true
            }
            R.id.nav_sched -> {
                navigateTo(ScreenType.SCHED)
                return true
            }
        }
        return false
    }
}

enum class ScreenType {
    MAP, PATIENT, SCHED
}
package com.covid19trackerapp

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import com.covid19trackerapp.ObjectSingleton.loginData
import com.covid19trackerapp.ObjectSingleton.mypass
import com.covid19trackerapp.databinding.ActivityEditProfileBinding
import com.covid19trackerapp.interfaces.PatientInterface
import com.covid19trackerapp.models.PatientProfileData
import com.covid19trackerapp.models.PatientUpdateData
import com.covid19trackerapp.presenters.PatientPresenter

class EditProfileActivity : AppCompatActivity(), PatientInterface.PatientInfoView, PatientInterface.UpdatePatientView {
    private val binding: ActivityEditProfileBinding by contentView(R.layout.activity_edit_profile)
    lateinit var loadingDialog: AppCompatDialog
    lateinit var presenter: PatientPresenter
    var sgenders = arrayOf(
        "Male",
        "Female"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = DialogFactory.showLoadingDialog(this@EditProfileActivity)
        presenter = PatientPresenter()
        presenter.patientView = this
        presenter.updateInfoView = this
        presenter.getPatientInfo(loginData!!.data!!.id.toString())
        val adapterGender: ArrayAdapter<String> = ArrayAdapter<String>(
            this,
            android.R.layout.select_dialog_singlechoice,
            sgenders
        )

        binding.run {
            backImage.setOnClickListener {
                finish()
            }
            autoTextGender.setText("Male")
            autoTextGender.threshold = 1
            autoTextGender.setAdapter(adapterGender)
            autoTextGender.setOnClickListener {
                autoTextGender.apply {
                    showDropDown()
                }
            }
            autoTextGender.setOnFocusChangeListener { view, b ->
                if(b) {
                    autoTextGender.apply {
                        autoTextGender.error = null
                        showDropDown()
                    }
                } else {
                    autoTextGender.apply {
                        autoTextGender.error = null
                        dismissDropDown()
                    }
                }
            }
            cancelBtn.setOnClickListener {
                finish()
            }
            updateBtn.setOnClickListener {
                validate()
            }
        }
    }

    fun validate() {
        binding.run {
            if(firstname.text.isNullOrEmpty() ||
                middlename.text.isNullOrEmpty() ||
                lastname.text.isNullOrEmpty() ||
                emailAddress.text.isNullOrEmpty() ||
                contactNumber.text.isNullOrEmpty() ||
                primaryAddress.text.isNullOrEmpty() ||
                secondaryAddress.text.isNullOrEmpty()) {
                toastValidation("All fields required")
            } else {
                var gender = 1
                if(autoTextGender.text.toString().toLowerCase() == "female") {
                    gender = 2
                }
                loadingDialog.show()
                presenter.updateProfile(
                    loginData!!.data!!.id.toString(),
                    firstname.text.toString(),
                    middlename.text.toString(),
                    lastname.text.toString(),
                    emailAddress.text.toString(),
                    mypass!!,
                    primaryAddress.text.toString(),
                    secondaryAddress.text.toString(),
                    contactNumber.text.toString(),
                    gender
                )
            }
        }
    }

    override fun onFailed(msg: String) {
        loadingDialog.dismiss()
        toastValidation(msg)
    }

    override fun onUpdatePatient(res: PatientUpdateData) {
        loadingDialog.dismiss()
        finish()
    }

    override fun onGetPatient(res: PatientProfileData) {
        loadingDialog.dismiss()
        if(res.gender_type == 1) {
            res.mygender = "Male"
        } else {
            res.mygender = "Female"
        }

        binding.run {
            model = res
            executePendingBindings()
        }
    }
}
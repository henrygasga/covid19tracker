package com.covid19trackerapp

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.res.use
import com.google.android.material.textfield.TextInputEditText


inline fun <reified T : Activity> Activity.launchActivity(extras: Bundle? = null, noinline block: Intent.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    if (extras != null) intent.putExtras(extras)
    intent.block()
    startActivity(intent)
}

fun Activity.toast(msg: String) {
//    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    Log.d("RIDERKOUSER","$msg")
}

@SuppressLint("ObsoleteSdkInt")
fun Activity.setTransparentStatusBar() {
    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.statusBarColor = Color.TRANSPARENT
    }
}

fun Activity.toastValidation(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}

fun AutoCompleteTextView.hintSetup(text: String) {
    this.hint = text
    this.setOnFocusChangeListener { v, hasFocus ->
        if(hasFocus) {
            this.hint = ""
        } else {
            this.hint = text
        }
    }
}

fun TextInputEditText.hintSetup(text: String) {
    this.hint = text
    this.setOnFocusChangeListener { v, hasFocus ->
        if(hasFocus) {
            this.hint = ""
        } else {
            this.hint = text
        }
    }
}

fun AppCompatEditText.hintSetup(text: String) {
    this.hint = text
    this.setOnFocusChangeListener { v, hasFocus ->
        if(hasFocus) {
            this.hint = ""
        } else {
            this.hint = text
        }
    }
}

@ColorInt
@SuppressLint("Recycle")
fun Context.themeColor(
    @AttrRes themeAttrId: Int
): Int {
    return obtainStyledAttributes(
        intArrayOf(themeAttrId)
    ).use {
        it.getColor(0, Color.MAGENTA)
    }
}

fun TextInputEditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun Context.getDrawableOrNull(@DrawableRes id: Int?): Drawable? {
    return if (id == null || id == 0) null else AppCompatResources.getDrawable(this, id)
}

fun Activity.getRootView(): View {
    return findViewById<View>(android.R.id.content)
}
fun Context.convertDpToPx(dp: Float): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        this.resources.displayMetrics
    )
}
fun Activity.isKeyboardOpen(): Boolean {
    val visibleBounds = Rect()
    this.getRootView().getWindowVisibleDisplayFrame(visibleBounds)
    val heightDiff = getRootView().height - visibleBounds.height()
    val marginOfError = Math.round(this.convertDpToPx(50F))
    return heightDiff > marginOfError
}

fun Activity.isKeyboardClosed(): Boolean {
    return !this.isKeyboardOpen()
}
package com.covid19trackerapp

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import com.covid19trackerapp.databinding.ActivityRegisterBinding
import com.covid19trackerapp.interfaces.PatientInterface
import com.covid19trackerapp.models.ResultRegister
import com.covid19trackerapp.presenters.PatientPresenter

class RegisterActivity : AppCompatActivity(), PatientInterface.RegisterView {
    private val binding: ActivityRegisterBinding by contentView(R.layout.activity_register)
    lateinit var loadingDialog: AppCompatDialog
    lateinit var presenter: PatientPresenter
    var genders = arrayOf(
        "Male",
        "Female"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = DialogFactory.showLoadingDialog(this@RegisterActivity)
        presenter = PatientPresenter()
        presenter.registerView = this

        val adapterGender: ArrayAdapter<String> = ArrayAdapter<String>(
            this,
            android.R.layout.select_dialog_singlechoice,
            genders
        )
        binding.run {
            backImage.setOnClickListener {
                finish()
            }
            autoTextGender.setText("Male")
            autoTextGender.threshold = 1
            autoTextGender.setAdapter(adapterGender)
            autoTextGender.setOnClickListener {
                autoTextGender.apply {
                    showDropDown()
                }
            }
            autoTextGender.setOnFocusChangeListener { view, b ->
                if(b) {
                    autoTextGender.apply {
                        autoTextGender.error = null
                        showDropDown()
                    }
                } else {
                    autoTextGender.apply {
                        autoTextGender.error = null
                        dismissDropDown()
                    }
                }
            }
            registerBtn.setOnClickListener {
                validate()
            }

        }
    }

    fun validate() {
        binding.run {
            if(firstname.text.isNullOrEmpty() ||
                    middlename.text.isNullOrEmpty() ||
                    lastname.text.isNullOrEmpty() ||
                    emailAddress.text.isNullOrEmpty() ||
                    contactNumber.text.isNullOrEmpty() ||
                    primaryAddress.text.isNullOrEmpty() ||
                    secondaryAddress.text.isNullOrEmpty() ||
                    pass.text.isNullOrEmpty() ||
                    confirmPass.text.isNullOrEmpty()) {
                toastValidation("All fields required")
            } else if(pass.text.toString() != confirmPass.text.toString()) {
                toastValidation("Password did not match.")
            } else {
                var gender = 1
                if(autoTextGender.text.toString().toLowerCase() == "female") {
                    gender = 2
                }
                loadingDialog.show()
                presenter.register(
                    firstname.text.toString(),
                    middlename.text.toString(),
                    lastname.text.toString(),
                    emailAddress.text.toString(),
                    pass.text.toString(),
                    primaryAddress.text.toString(),
                    secondaryAddress.text.toString(),
                    contactNumber.text.toString(),
                    gender
                )
            }
        }

    }

    override fun onFailed(msg: String) {
        loadingDialog.dismiss()
        toastValidation(msg)
    }

    override fun onRegisterSuccess(res: ResultRegister) {
        loadingDialog.dismiss()
        toastValidation("Successful registered ${res.firstname}")
        finish()
    }
}
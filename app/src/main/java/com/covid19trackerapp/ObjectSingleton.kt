package com.covid19trackerapp

import com.covid19trackerapp.models.LoginResult

object ObjectSingleton {
    var loginData: LoginResult? = null
    var mypass: String? = ""
    var myIp = ""
}
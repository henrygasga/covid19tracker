package com.covid19trackerapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import com.covid19trackerapp.ApiHelper.baseUrl
import com.covid19trackerapp.ObjectSingleton.loginData
import com.covid19trackerapp.ObjectSingleton.mypass
import com.covid19trackerapp.Utils.Companion.myserver
import com.covid19trackerapp.databinding.ActivityLoginBinding
import com.covid19trackerapp.interfaces.PatientInterface
import com.covid19trackerapp.models.LoginResult
import com.covid19trackerapp.presenters.PatientPresenter

class LoginActivity : AppCompatActivity(), PatientInterface.LoginView {

    private val binding: ActivityLoginBinding by contentView(R.layout.activity_login)
    lateinit var loadingDialog: AppCompatDialog
    lateinit var presenter: PatientPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = DialogFactory.showLoadingDialog(this@LoginActivity)
        presenter = PatientPresenter()
        presenter.loginView = this

        binding.run {
            signUp.setOnClickListener {
                launchActivity<RegisterActivity> {  }
            }
            loginBtn.setOnClickListener {
                validate()
            }
        }
    }

    fun validate() {
        binding.run {
            if(email.text.isNullOrEmpty() ||
                pass.text.isNullOrEmpty()) {
                toastValidation("All fields required")
            } else {
                loadingDialog.show()
                presenter.login(
                    email.text.toString(), pass.text.toString()
                )
            }
        }
    }

    override fun onFailed(msg: String) {
        loadingDialog.dismiss()
        toastValidation(msg)
    }

    override fun onLoginSuccess(res: LoginResult) {
        loadingDialog.dismiss()
        loginData = res
        mypass = binding.pass.text.toString()
        toastValidation("Welcome ${res.data!!.firstname}!")
        launchActivity<DashboardActivity> {  }
        finish()
    }
}
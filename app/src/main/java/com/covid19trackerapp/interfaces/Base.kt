package com.covid19trackerapp.interfaces

interface Base {
    fun onFailed(msg: String)
}
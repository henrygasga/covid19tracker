package com.covid19trackerapp.interfaces

import com.covid19trackerapp.models.*
import com.google.android.gms.maps.model.LatLng

interface PatientInterface {
    interface LoginView : Base {
        fun onLoginSuccess(res: LoginResult)
    }
    interface RegisterView : Base {
        fun onRegisterSuccess(res: ResultRegister)
    }
    interface PatientInfoView : Base {
        fun onGetPatient(res: PatientProfileData)
    }
    interface ChangePassView : Base {
        fun onChangePass()
    }
    interface UpdatePatientView : Base {
        fun onUpdatePatient(res: PatientUpdateData)
    }
    interface QuarantineSchedView : Base {
        fun onGetQuarantineSched(res: QuarantineSchedData)
    }
    interface LocationCreateView : Base {
        fun onSendLocation(res: SendLocationData)
    }
    interface NotificationView : Base {
        fun onGetNotification()
    }
    interface GeomappingRadiusLocationsView : Base {
        fun onGeomappingRadiusLocations(res: GeoMappingRadiusRes)
    }
    interface CreateNotificationView : Base {
        fun onCreateNotification(res: NotifCreateData)
    }
    interface Presenter {
        fun createNotification(
            address: String,
            latLng: LatLng,
            is_outside: Int,
            patient_id: Int,
            geomapping_radius_location_id: Int
        )
        fun getGeomappingRadiusLocations()
        fun getPatientsNotification(id: String)
        fun sendLocation(
            latLng: LatLng,
            address: String,
            patient_id: Int,
            patient_quarantine_schedule_id: Int
        )
        fun getQuarantineSched()
        fun updateProfile(
            id: String,
            firstname: String,
            middlename: String,
            lastname: String,
            email: String,
            password: String,
            primary_address: String,
            secondary_address: String,
            contact_no: String,
            gender_type: Int
        )
        fun changePassword(
            id: String,
            old_password: String,
            new_password: String
        )
        fun getPatientInfo(
            id: String
        )
        fun register(
            firstname: String,
            middlename: String,
            lastname: String,
            email: String,
            password: String,
            primary_address: String,
            secondary_address: String,
            contact_no: String,
            gender_type: Int
        )

        fun login(
            email: String,
            password: String
        )
    }
}
package com.covid19trackerapp

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.covid19trackerapp.databinding.ActivityMainBinding
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private val binding: ActivityMainBinding by contentView(R.layout.activity_main)
    private lateinit var mMap: GoogleMap
    private lateinit var mapFragment: SupportMapFragment
    private var mLocationRequest: LocationRequest? = null
    private var mLocationCalback: LocationCallback? = null
    lateinit var fusedClient: FusedLocationProviderClient
    var myLoc: Location? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedClient = LocationServices.getFusedLocationProviderClient(this@MainActivity)
        mLocationRequest = LocationRequest.create()
        mLocationRequest!!.interval = 60000
        mLocationRequest!!.fastestInterval = 5000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        mLocationCalback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                if (p0 == null) {
                    return
                }
                for(loc:Location in p0.locations) {
                    if (loc != null) {
//                        toastValidation("lat: ${loc.latitude}, lng: ${loc.longitude}")
                        myLoc = loc
                        setLocation()
                        break
                    }
                }
            }
        }

        binding.run {
            mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
            mapFragment.getMapAsync(this@MainActivity)
        }
    }

    override fun onResume() {
        super.onResume()
        fetchLastLocation()
    }

    fun fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            initLocationPermission()
            return
        }
        fusedClient.lastLocation.addOnSuccessListener {
            if (it != null) {
                myLoc = it
                setLocation()
            } else {
                fusedClient.requestLocationUpdates(mLocationRequest, mLocationCalback!!, null)
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isZoomGesturesEnabled = true
        mMap.uiSettings.isScrollGesturesEnabled = true
        mMap.uiSettings.isRotateGesturesEnabled = false
        mMap.uiSettings.isZoomControlsEnabled = false

    }

    fun setLocation() {
        mMap.setOnMapLoadedCallback {
            if(myLoc != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(myLoc!!.latitude, myLoc!!.longitude), 18f))
                mMap.addMarker(
                    MarkerOptions()
                    .position(LatLng(myLoc!!.latitude, myLoc!!.longitude))
                    .title("You are here!"))
            }
        }
    }

    private fun initLocationPermission() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        fetchLastLocation()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    showRationalDialogForPermissions()
                }
            }).check()
    }

    private fun showRationalDialogForPermissions() {
//        if(!(applicationContext as Activity).isFinishing) { // here fix
        AlertDialog.Builder(this@MainActivity)
            .setMessage(R.string.go_permission_settings)
            .setPositiveButton(R.string.str_go_settings) { dialog, which -> openAppPermissionSettings() }
            .setNegativeButton(R.string.cancel) { dialog, which -> dialog.dismiss() }.show()
//        }

    }

    protected fun openAppPermissionSettings() {
        try {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        when (requestCode) {
            123 -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] === PackageManager.PERMISSION_DENIED) {
                    // permission was denied, show alert to explain permission
                    showRationalDialogForPermissions()
                } else {
                    //permission is granted now start a background service
                    if (ActivityCompat.checkSelfPermission(
                            applicationContext,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(
                            applicationContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        // permison accept
                        fetchLastLocation()
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
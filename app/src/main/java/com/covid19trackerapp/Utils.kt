package com.covid19trackerapp

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Utils {
    companion object {
        var myserver = ""
        @JvmStatic
        fun convertDateWithTimeZone(time: String): String {
            var formattedTime = ""
            try {
                val sdf: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                sdf.timeZone = TimeZone.getTimeZone("GMT")
                val sdf12: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault())
                sdf12.timeZone = TimeZone.getTimeZone("GMT+04:00")
                val dObj: Date = sdf.parse(time)
                formattedTime = sdf12.format(dObj)
            } catch (e: ParseException) {

            }
            return formattedTime
        }
    }
}